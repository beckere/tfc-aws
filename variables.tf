variable "AWS_ACCESS_KEY"{
  description = "AWS access key"
  type        = string
}

variable "AWS_SECRET_KEY"{
  description = "AWS secret key"
  type        = string
}

variable "region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string
}

variable "namespace" {
  description = "Project namespace"
  default     = "test"
  type        = string
}

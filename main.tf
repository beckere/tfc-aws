module "networking" {
  source    = "./modules/networking"
  namespace = var.namespace
}

module "compute" {
  source     = "./modules/compute"
  namespace  = var.namespace
  vpc        = module.networking.vpc
  sg_pub_id  = module.networking.sg_pub_id
  sg_priv_id = module.networking.sg_priv_id
  # key_name   = module.ssh-key.key_name
}
